


//  JSON

/*"cities" : [
	{ 

		"city" : "Quezon City", 
		"province" : "Metro Manila",
		"country" : "Philippines"

	}

	{ 

		"city" : "Manila City",
		"province" : "Metro Manila",
		"country" : "Philippines"

	}

	{ 

		"city" : "Makati City",
		"province" : "Metro Manila",
		"country" : "Philippines"

	}


]*/

let batchesArr = [

	{
		batchName: "Batch217",
		schedule: "Full-Time"
	},
	{
		batchName: "Batch218",
		schedule: "Part-Time"
	}

]

// stringify

console.log("Result from stringify method");
console.log(JSON.stringify(batchesArr));



let data = JSON.stringify({

	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines"
	}
});

console.log(data);

// Using stringify() with variables

let firstName = prompt("What is your First Name?"); 
let lastName = prompt("What is your Last Name?");
let age = prompt("What is your age?");

let address = {

	city: prompt("Which city do you live in?"),

	country: prompt("Which country does your city address belong to?")

}

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address

})


console.log(otherData);

// Converting Stringified JSON into JS Objects

// Parse Method


let batchesJSON = `[
	{
		"batchName" : "Batch 217",
		"schedule" : "Full-Time"
	},
	{
		"batchName" : "Batch 218",
		"schedule" : "Part-Time"
	}
]`


console.log("Result from parse method");
console.log(JSON.parse(batchesJSON));



let stringifiedObject = `{

	"name" : "John",
	"age" : "31",
	"address" : {
		"city" : "Manila",
		"country" : "Philippines"
	}
}`

console.log(JSON.parse(stringifiedObject));